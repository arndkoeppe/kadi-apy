# Release history

## 0.46.0 (Unreleased)

**Additions**

* Added a flag to the `json_to_kadi` conversion function to automatically
  convert integer values which are not in the value range supported by Kadi to
  strings instead and a parameter to specify a default fallback value for
  `null` values.
* Added a new CLI command `show-info` in templates to print information of a
  template.
* Added `shacl` export type in templates `export` CLI command.
* Added a public `utils` module for various utility functionality.

**Changes**

* Changed the underlying HTTP library from `requests` to `niquests`. Note that
  this should not affect any existing functionality.
* Removed the deprecated `token` parameter of the `KadiManager` class.
* Removed `pipe` option for metadata in the `show-info` CLI command for
  records. The `export-metadata` command can be used as a possible replacement.
* Made the file path optional in the `download_file` method of records to allow
  manually streaming the data.

**Fixes**

* Fixed `pipe` option not working for `export-metadata` CLI command in records.

## 0.45.0 (2024-12-05)

**Changes**

* Updated the ELN/RO-Crate import to make use of several improvements
  introduced in Kadi4Mat version 1.2.0.

## 0.44.0 (2024-11-26)

**Changes**

* Updated `import_json_schema` method to support custom `propertiesOrder`
  keyword for files exported via Kadi4Mat.
* Updated `import_shacl` method to preserve the ordering of extra metadata.
* Updated `import_eln` method to support handling the flattened structure of
  the metadata file of RO-Crates.

**Fixes**

* Fixed an incorrect handling of generic metadata in the `import_eln` method.

## 0.43.0 (2024-10-28)

**Additions**

* Added JSON export functionality for record extra metadata.

**Changes**

* Updated the `import_eln` method to support importing generic metadata from
  the metadata file of RO-Crates.
* Dropped support for Python 3.8 and added support for Python 3.13.
* JSON-based query parameters, e.g. the resource export filter, will now be
  URL-encoded automatically, if applicable.
* Improved the JSON Schema import functionality for handling the properties
  listed in the `required` array.

## 0.42.0 (2024-09-12)

**Additions**

* Added a `kadi_to_json` conversion function to convert Kadi4Mat-compatible
  extra metadata into a plain structure.

## 0.41.0 (2024-07-24)

**Changes**

* Adjusted the use of certain API endpoints according to changes introduced in
  Kadi4Mat version 0.52.0
* Changed all requests to use the future stable API version `v1` of Kadi4Mat.

## 0.40.1 (2024-07-18)

**Fixes**

* Fixed the JSON-Schema import not working anymore when using value ranges.

## 0.40.0 (2024-07-02)

**Additions**

* Added an `import_shacl` method to the miscellaneous library functionality and
  `misc import-shacl` command in CLI for importing SHACL Shapes files.
* Added a `ca_bundle` parameter to the `KadiManager` class to overwrite custom
  CA bundles specified via the config file.

**Changes**

* Deprecated the `token` parameter of the `KadiManager` class in favor of `pat`
  for consistency with the config file.

**Fixes**

* Fixed config file parsing logic to always prioritize parameters passed
  directly to the `KadiManager` class.

## 0.39.1 (2024-05-06)

**Fixes**

* Fixed handling of the `Retry-After` header when a rate limit has been
  exceeded.

## 0.39.0 (2024-03-21)

**Additions**

* The command `kadi-apy collections show-info` can now list all
  child-collections. It also prints the number of all found resources.
* All requests now include a custom `User-Agent` header by default.

**Changes**

* The `KadiManager` class now uses a session to handle all requests.

## 0.38.0 (2024-02-26)

**Additions**

* Added a new configuration option `whitelist` to specify hosts that kadi-apy
  is allowed to redirect to.

**Fixes**

* Fixed the JSON-Schema import not working for properties without an explicit
  type and empty array properties.

## 0.37.0 (2024-02-12)

**Additions**

* Added a warning for POSIX-compliant systems when the permissions of the
  config file allow reading/writing by users other than the file owner.
* Added a `json_to_kadi` conversion function to convert plain JSON to a
  Kadi4Mat-compatible extra metadata structure.

**Changes**

* The permissions of config files created on POSIX-compliant systems via the
  CLI will now be more restrictive by default.

## 0.36.0 (2024-01-24)

**Additions**

* Added an `import_json_schema` method to the miscellaneous library
  functionality and `misc import-json-schema` command in CLI for importing
  JSON-Schema files.

**Changes**

* Renamed `misc import-file` to `misc import-eln` to have a separate CLI
  command for importing RO-Crate files.
* Updated the file upload functionality to use the new upload API introduced in
  Kadi4Mat version 0.45.0.

## 0.35.0 (2024-01-10)

**Additions**

* Added a verification for SHA256 checksums when using the `import_eln` method
  if the corresponding property exists.

**Changes**

* Updated the `import_eln` method to also support comma-separated keywords.
* File uploads now use a fixed size of 50 MB to decide whether a direct or
  chunked upload will be performed.

**Fixes**

* Fixed descriptions of existing files being overwritten with an empty string
  when uploading files via the CLI without specifying a description.

## 0.34.0 (2023-10-25)

**Additions**

* Added new CLI commands `add_collection_link` and `remove_collection_link` in
  collections to link and remove a child collection to/from a parent
  collection.
* Added new CLI commands `add_collection_link` and `remove_collection_link` in
  records to link and remove a record to/from a collection.

**Changes**

* Changed the RO-Crate import to not require a publisher entity anymore.

**Fixes**

* Fixed the direct upload functionality to honor the `force` parameter instead
  of always replacing files.

## 0.33.0 (2023-10-04)

**Additions**

* Added new CLI commands `add_user`, `remove_user`, `add_group_role` and
  `remove_group_role` for templates.
* Added additional handling when importing RO-Crate files exported via
  Kadi4Mat.
* Removed the `__version__` attribute in favor of using
  `importlib.metadata.version("kadi-apy")`.
* Added support for Python 3.12.

## 0.32.0 (2023-08-29)

* Adjusted use of the `displayname` attribute of user identities according to
  API changes in Kadi4Mat version 0.40.0.
* Added a `term_iri` parameter to specify IRI of an existing term for record
  link functions in both library and CLI.
* Exposed additional constants as well as all exceptions as part of the main
  package.

## 0.31.0 (2023-07-18)

* Changed the `add_tag` library method to always return a response object.
* Redirects won't be followed automatically anymore, since the Authorization
  header containing the access token would be stripped away anyways. If a
  redirect is detected now, a corresponding exception will be thrown to alert
  about the necessary code/configuration change.
* Removed support for Python 3.7.

## 0.30.0 (2023-06-14)

* Added support for Python 3.11.

## 0.29.0 (2023-05-30)

* Added an `import_eln` method to the miscellaneous library functionality for
  importing RO-Crate files following the "ELN" file specification.
* Removed the `pat_user_id` property of the manager class, since it can already
  be retrieved using `pat_user`.
* Renamed the `miscellaneous` command group to just `misc`.
* Refactored the search functionality of the library. All methods are now
  provided as part of a single class usable via the `KadiManager.search`
  property. Additionally, the methods `search_items` and `search_items_user`
  have been renamed to `search_resources` and `search_user_resources`,
  respectively.
* Added a general `misc import-file` command to import files.

## 0.28.1 (2023-05-16)

* Fixed response data not being streamed when potentially exporting large
  amounts of data, such as RO-Crates.

## 0.28.0 (2023-04-19)

* Fixed the creation of templates not working via the corresponding CLI
  command.
* Added the `rdf` export type to the corresponding CLI commands of records and
  collections.

## 0.27.0 (2023-03-14)

* Made the methods and commands to add metadata more robust in regards to the
  expected metadata structure.
* The full error response payload is now printed in CLI commands when using the
  `debug` verbose level.
* Added a `get_collections` function to retrieve collections linked with a
  collection id.
* Added a `add_collection_link` function to link a child collection to a
  parent collection.
* Added a `remove_collection_link` function to remove a child connection
  linked to a parent collection.
* Added a `download_all_files` method to download all the files in a record
  as a ZIP archive
* Added methods to query revisions of all the resources.

## 0.26.0 (2023-01-09)

* Added a `file_description` parameter for file upload functions in both
  library and CLI.
* Improved the `upload_file` method for uploading smaller files without the
  need for chunking.
* Added the `ro-crate` export type to the corresponding CLI commands of records
  and collections.

## 0.25.0 (2022-10-14)

* Improved the `get_metadatum` methods for records to retrieve nested metadata,
  which also now works in the corresponding CLI command.
* The record library version of `get_metadatum` does not use the `information`
  parameter anymore and returns `None` in case the requested metadatum was not
  found.
* Added functionality to retrieve templates shared with a group via the
  library.
* Added functionality to manage user and group roles of templates via the
  library.
* Added functionality to retrieve group roles of all resources via the library.

## 0.24.0 (2022-08-22)

* Fixed retrieval of deleted resources via the CLI.
* Fixed usage of some CLI related classes outside of builtin commands.
* Fixed purging of some resource types via the CLI.
* Deleted templates can now be purged and restored via the CLI.
* Added functionality to export templates.
* All export functions and commands now default to the JSON export type.

## 0.23.0 (2022-04-27)

* Added a `license` option to the `records edit` CLI subcommand.
* Removed support for Python 3.6.

## 0.22.0 (2022-03-07)

* The unit does not have to be specified in the dict using the `add_metadatum`
  function within the CLI/lib.

## 0.21.0 (2022-01-17)

* Removed the `click_completion` dependency. Autocompletion is now only
  supported for bash, fish and zsh.
* Fixed the export function via using the correct parameter.
* Added handling of the `ConnectionError`.
* Allow the `search_pagination_options` decorator to set an user-specific
  description for the option `page`.
* Refactoring of the record linkage, as the check for identical links is
  already carried out in Kadi.
* Refactoring of the export function. The file is now already stored using the
  export function defined in the lib part.
* Add an example if metadata have not the right input format when using the
  `add_metadata` (CLI) function.

## 0.20.0 (2021-11-26)

* Added support for new collaborator role.
* Added exclude options and a query parameter in the `get-records` and
  `get-collections` CLI commands.
* Added utility function to create identifier from arbitrary string input.

## 0.19.0 (2021-11-24)

* Increased the upload speed of small files.
* Add option to pipe a list of downloaded files as tokenlist.
* Relaxed required Python version again.
* Add functions to use the `GET /api/tags` endpoint.

## 0.18.0 (2021-11-05)

* Include functions to export a collection.
* Changed default separator in `search_resource_ids` to `,`.
* Add option so search records in child collections.
* Include option to work with tokenlists.

## 0.17.1 (2021-11-04)

* Added feature to export a record to a folder based on the identifier.

## 0.17.0 (2021-11-04)

* Include methods using the export endpoint.
* Improve usability of config add-instance.
* Use of more mixin classes in the CLI lib.
* Add AUTHORS.md.
* Add the GET /api/roles endpoint.
* Add CLI function to edit basic metadata of a template.
* Replaced hide-public with visibility parameter.
* Refactoring the use of raise_request_error().
* Renamed all `replace` keywords to `force`.
* Added a `get_metadatum` method to the lib record.
* Added functions to update a record link (name).
* Added options to use a tokenlist in the search.
* Take into account corner case if only a scope to create a resource is given.
* Make get_metadatum work with nested types.

## 0.16.0 (2021-07-14)

* Include search function which returns a list of ids.
* Include CLI tools to search for record and collection ids.

## 0.15.0 (2021-06-24)

* Add factory methods for base functions into CLIKadiManager.
* Do not validate metadatum if value is `None`.

## 0.14.0 (2021-06-17)

* Improve documentation.

## 0.13.0 (2021-06-08)

* Add option to store record files in a folder based on the record's
  identifier.
* Add basic validation for host input.
* Add more CLI config tools.

## 0.12.0 (2021-06-01)

* Doc improvements.
* Add option to use the KadiManager via the '@apy_command' decorator.

## 0.11.1 (2021-05-18)

* Smaller refactoring in docs.
* Fix bash example script.
* Show also username in user search.
* Set identity type to required in case of specifying user via username.

## 0.11.0 (2021-05-11)

* Include docs.
* Refactor verbose.
* Introduce a `CLIKadiManager`.

## 0.10.0 (2021-04-29)

* Update `README.md`.
* Changed two required arguments of `upload_file` method of `CLIRecord` class
  to optional.
* Include verbose level to manage the amount of print outputs.

## 0.9.1 (2021-04-23)

* Add pypi deploy runner.

## 0.9.0 (2021-04-22)

* Add CLI tools to configure the config file.
* Move `get-kadi-info` from CLI group `miscellaneous` to `config`.

## 0.8.0 (2021-04-12)

* Add default value in `records get-metadatum`.
* Add `get_file_info` function for records.
* Return list of downloaded file(s) in `get_file`
* Add experimental CLI tool to download and execute a workflow.
* Add CLI tool `kadi-apy miscellaneous get-kadi-info`.
* Exit with error code 1 during uploading a file in case the file already
  exists and should not be replaced.

## 0.7.0 (2021-03-24)

* Add `exit-not-created` flag to CLI create tools.
* Fix order in which `_meta` is invalidated.
* Add option to download only those files from record matching pattern.
* Add char options for decorator to define options of files and resources.
* Add CLI tool `add-string-as-file`.

## 0.6.0 (2021-02-26)

* Add option to use certificates from ca_bundle.
* Add option skip the initial request.

## 0.5.0 (2021-02-24)

* Include a config file to store the information about host and PAT.
* Added timeout config option.
* Add autocompletion.

## 0.4.1 (2021-02-02)

* Include the --version output.
* Add handling for missing schema exception.
* Unify the behavior of decorators if information is not required.
* Add --xmlhelp runner

## 0.4.0 (2021-01-22)

* Add option to hide public resources in the CLI search.
* Functions to handle miscellaneous (license search, remove item from trash).
* Add function to remove all metadata from a record.

## 0.3.3 (2021-01-14)

* Smaller cleanups.

## 0.3.2 (2021-01-14)

* Include missing `__init__.py`.

## 0.3.1 (2021-01-14)

* Include xmlhelpy import from pypi.

## 0.3.0 (2021-01-14)

* Include definition of public api.
* Integration of --xmlhelp option to all CLI tools.
* Definition of CLI classes which can be used in additions tools.
* Print more infos when using the CLI.
* Various refactoring.

## 0.2.3 (2020-10-23)

* Fix for specifying PAT and host directly in certain cli commands.

## 0.2.2 (2020-10-23)

* Raise exception instead of sys.exit(1) for cli functions.

## 0.2.1 (2020-10-05)

* Update examples.

## 0.2.0 (2020-10-02)

* Work with identifiers for items.

## 0.1.2 (2020-09-25)

* Include group roles for records and collections.
* Add option to skip ssl/tls cert verification.

## 0.1.1 (2020-09-21)

* Improved printing for updated metadata.
* Smaller improvements.

## 0.1.0 (2020-09-09)

* Most API endpoints for managing records, collections and groups, as well as
  some others, are usable directly through the library.
* Most of that functionality is also exposed via CLI by using the kadi-apy
  command.
